# sysinfo_server

Golang REST API application exposing GET endpoint reporting kernel + userspace bootup time

# how to build

$ go build sysinfo_server.go

# Typical output

(classic)f843d0@ubuntu:~/sysinfo_server$ ./sysinfo_server&
[1] 5004
(classic)f843d0@ubuntu:~/sysinfo_server$ Server ready, endpoints: /version and /duration

(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/
Please contact endpoints /version or /duration
(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/version
0.1
(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/duration
20.973s
(classic)f843d0@ubuntu:~/sysinfo_server$ 




(classic)f843d0@ubuntu:~/sysinfo_server$ ./sysinfo_server --json&
[1] 5019
(classic)f843d0@ubuntu:~/sysinfo_server$ Server ready, endpoints: /version and /duration

(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/
Please contact endpoints /version or /duration
(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/version
{
	"version":"0.1"
}
(classic)f843d0@ubuntu:~/sysinfo_server$ curl http://localhost:8080/duration
{
	"duration":"20.973s"
}

