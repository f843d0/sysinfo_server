package main

import (
	"log"
	"os/exec"
	"net/http"
	"fmt"
	"regexp"
	"os"
)

var isJsonActivated bool

func HandlerVersion(w http.ResponseWriter, r *http.Request) {
	if (isJsonActivated) {
		fmt.Fprintf(w, "{\n\t\"version\":\"")
	}
	fmt.Fprintf(w, "0.1")
	if (isJsonActivated) {
		fmt.Fprintf(w, "\"\n}")
	}
	fmt.Fprintf(w, "\n")
}

func HandlerRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Please contact endpoints /version or /duration\n")
}

func HandlerDuration(w http.ResponseWriter, r *http.Request) {
	cmd := "systemd-analyze"
	out, err := exec.Command(cmd).Output()
	if err != nil {
		fmt.Fprintf(w, "%s exec command returned error, this is severe (or a sysvinit based system :P)\n", cmd)
		return
	}
	if (isJsonActivated) {
		fmt.Fprintf(w, "{\n\t\"duration\":\"")
	}
	reg := regexp.MustCompilePOSIX(`[0-9]+\.[0-9]+s$`)
	fmt.Fprintf(w, "%s", string(reg.Find(out)))
	if (isJsonActivated) {
		fmt.Fprintf(w, "\"\n}")
	}
	fmt.Fprintf(w, "\n")
}

func main() {
	if (len(os.Args) > 2 ) {
		fmt.Println(os.Args[0], "accepts at best --json as argument")
		os.Exit(1)
	}
	if (len(os.Args) == 2) {
		if (os.Args[1] == "--json") {
			isJsonActivated = true
		} else {
			fmt.Println(os.Args[0], "accepts at best --json as argument")
			os.Exit(1)
		}
	}
	http.HandleFunc("/version", HandlerVersion)
	http.HandleFunc("/duration", HandlerDuration)
	http.HandleFunc("/", HandlerRoot)
	fmt.Println("Server ready, endpoints: /version and /duration")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
